#!/bin/env.python
#living script to find certs and check expriation on them. Testing still in progress.

import time
import paramiko
import sys
import base64
import re
#------You need to enter in this information-------------
username = 'x221647'    
password = base64.b64decode('S3lyYWJvMTk=')
#------------------------------The part above is very important------------
nbytes = 4096
port = 22
cmd_SWA_cert = "cd /SWA/mxmtxid/certs/ ; find . -name 'SWADEVROOT*.cer' -exec openssl x509 -enddate -noout -in {} \;"

cmd_SWA_QA = "cd /SWA/mxmtxid/certs/ ; find . -name 'WQISSUINGCA*.cer' -exec openssl x509 -enddate -noout -in {} \;"

cmd_solr_cert = "cd /SWA/mxmtxid/certs ; find . -name 'SWADEVISS*.cer' -exec openssl x509 -enddate -noout -in {} \;"  #this is correct 

cmd_dev_asb = "cd /SWA/mxmtxid/asb-3.5.0.7*/server ; find . -maxdepth 1 -name 'SWADEVI*.cer' -exec openssl x509 -enddate -noout -in {} \;" #this is also correct
cmd_itest_asb = "cd /SWA/mxmtxid/asb-3.5.0.7*/server ; find . -maxdepth 1 -name 'SWADEVI*.cer' -exec openssl x509 -enddate -noout -in {} \;"
cmd_qa_asb = "cd /SWA/mxmtxid/asb-3.5.0.7*/server ; find . -maxdepth 1 -name 'maintenix-qa*.cert' -exec openssl x509 -enddate -noout -in {} \;"

cmd_jasper_cert = "print 'jasper cert!'"

print 'variables added'
print 'todays date: ',
time = time.strftime("Month:%m Day:%d---Time:%H:%M:%S:----Year:%Y")
print time
print'-----------------------------------------------------'
#---------------------------------all mtx environments------------------
env_mappings_mtx_all = {
    'dev':{
        'dev1':['xldmxishr05'],
        'dev2':['xldmxishr06'],
        'dev3':['xldmxishr11'],
        'dev4':['xldmxishr13']
        },
    'itest-shr':{
        'itest1':['xltmxishr01', 'xltmxishr02'],
        'itest2':['xltmxishr03', 'xltmxishr04'],
        'itest3':['xltmxishr05', 'xltmxishr06'],
        'itest4':['xltmxishr07', 'xltmxishr08'],
        'itest5':['xltmxishr09', 'xltmxishr10'],
    },
    'itest-srch':{
        'itest1':['xltmxisrch01', 'xltmxisrch02'],
        'itest2':['xltmxisrch03', 'xltmxisrch04'],
        'itest3':['xltmxisrch05', 'xltmxisrch06'],
        'itest4':['xltmxisrch07', 'xltmxisrch08'],
        'itest5':['xltmxisrch09', 'xltmxisrch10'],
    },
    'qa-shr':{
        'qa1':['xlqmxishr01', 'xlqmxishr02'],
	    'qa2':['xlqmxishr03', 'xlqmxishr04'],
	    'qa3':['xlqmxishr05', 'xlqmxishr06', 'xlqmxishr07', 'xlqmxishr08'],
	    'qa4':['xlqmxishr09', 'xlqmxishr10'],
	}
}

env_mappings_mtx = {
    'dev':{
        'dev1':['xldmxishr05'],
        'dev2':['xldmxishr06'],
        'dev3':['xldmxishr11'],
        'dev4':['xldmxishr13']
        },
    'itest-srch':{
        'itest1':['xltmxishr01', 'xltmxishr02'],
        'itest2':['xltmxishr03', 'xltmxishr04'],
        'itest3':['xltmxishr05', 'xltmxishr06'],
        'itest4':['xltmxishr07', 'xltmxishr08'],
        'itest5':['xltmxishr09', 'xltmxishr10'],
        }
}
#-------------------Cert Checker Body-------------------------------------------------------------

def certs_checker(hostname, environment):
    fail = 0
    client = paramiko.Transport((hostname,port)) 
    client.connect(username=username, password=password)
    #print 'logging in'
    #print ' connection made'
    print 'Environment: %s; Hostname: %s; ' % (environment, hostname)
#------------------------SWA SWA certs--
    session = client.open_channel(kind='session')
    if "shr" in hostname:
        SWA_certs(session, hostname)

#-----------------solr check------
    #print command1
    session = client.open_channel(kind='session')
    if "srch" in hostname or "xld" in hostname:
        #print "dev or itest sol"
        solr_cert(session, hostname)
    #else:
        #print 'Does not have solr'
#------------------------asb cert-------
    session = client.open_channel(kind='session')
    if "shr" in hostname:
        asb_cert(session, hostname)
        #print "asb cert"
#-----------------------------Jasper Cert---------
    session = client.open_channel(kind='session')
    if "xld" in hostname or "rpt" in hostname or "xltmxishr" in hostname:
        #print "jasper cert"
        jasper_cert(session, hostname)
    
        


    #print 'next server'
    print '-----------------------------------------------------'
    #print ' '
#------------------------------end certs_checker definition-------------



#---------------SWA certs check--------------------------
def SWA_certs(session, hostname):
    stdout_data = []
    stderr_data = []
    fail = 0
    if 'xld' in hostname:
        session.exec_command(cmd_SWA_cert)
    if 'xlt' in hostname:
        session.exec_command(cmd_SWA_cert)
    if 'xlq' in hostname:
        session.exec_command(cmd_SWA_QA)
    #print 'Session command'
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 5:
                break
    # print environment
    #print 'exit status: ', session.recv_exit_status()
    SWA_cert = ''.join(stdout_data)
    SWA_cert.split()
    #SWA_cert = SWA_cert.replace(' ','')
    print 'SWA cert: '
    if SWA_cert == '':
        print 'No Cert found'
    else:
        print SWA_cert,
        #print ' '

#-------------------------solr Check method--------------------
def solr_cert(session, hostname):
    stdout_data = []
    stderr_data = []
    fail = 0
    #print cmd_solr_cert
    session.exec_command(cmd_solr_cert)
    #print 'Session command'
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 5:
                break
    # print environment
    #print 'exit status: ', session.recv_exit_status()
    solr_cert = ''.join(stdout_data)
    solr_cert.split()
    #solr_cert = solr_cert.replace(' ','')
    print 'Solr cert: ',
    if solr_cert == '':
        print 'No Cert found'
    else:
        print solr_cert,
        #print ' '

#--------------------asb Cert Method-----------------------
def asb_cert(session, hostname):
    stdout_data = []
    stderr_data = []
    fail = 0
    if 'xld' in hostname:
        session.exec_command(cmd_dev_asb)
    if 'xlt' in hostname:
        session.exec_command(cmd_itest_asb)
    if 'xlq' in hostname:
        session.exec_command(cmd_qa_asb)
    #print command2
    while True:
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 5:
                break
    asb_cert = ''.join(stdout_data)
    print "Asb Cert: "
    if asb_cert == '':
        print 'No Cert found'
    else:
        print asb_cert,
        #print ' '
    
    
  
#---------------------jasper cert method---------------------------------
def jasper_cert(session, hostname):
    stdout_data = []
    stderr_data = []
    fail = 0
    #print hostname
    session.exec_command(cmd_jasper_cert)
    #print command3
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 5:
                break
    jasper_cert = ''.join(stdout_data)
    print "Jasper Certs: ",
    print 'jasper cert in keystore'
#----------------------MAIN-----------------------------------------------------------------

for stage, environments in env_mappings_mtx_all.iteritems():
    for environment, hosts in environments.iteritems():
        for host in hosts:
            certs_checker(host, environment)







