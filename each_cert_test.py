#!/bin/env.python
#living script to find certs and check expriation on them. Testing still in progress.

import time
import paramiko
import sys
import base64
import re
import datetime
from datetime import timedelta
#------You need to enter in this information-------------
username = 'x221647'    
password = base64.b64decode('S3lyYWJvMTk=')
#------------------------------The part above is very important------------
nbytes = 4096
port = 22

#---------------------------------all mtx environments------------------
env_mappings_mtx_all = {
    'dev':{
        'dev1':['xldmxishr05'],
        'dev2':['xldmxishr06'],
        'dev3':['xldmxishr11'],
        'dev4':['xldmxishr13']
        },
    'itest-shr':{
        'itest1':['xltmxishr01', 'xltmxishr02'],
        'itest2':['xltmxishr03', 'xltmxishr04'],
        'itest3':['xltmxishr05', 'xltmxishr06'],
        'itest4':['xltmxishr07', 'xltmxishr08'],
        'itest5':['xltmxishr09', 'xltmxishr10'],
    },
    'itest-srch':{
        'itest1':['xltmxisrch01', 'xltmxisrch02'],
        'itest2':['xltmxisrch03', 'xltmxisrch04'],
        'itest3':['xltmxisrch05', 'xltmxisrch06'],
        'itest4':['xltmxisrch07', 'xltmxisrch08'],
        'itest5':['xltmxisrch09', 'xltmxisrch10'],
    },
    'qa-shr':{
        'qa1':['xlqmxishr01', 'xlqmxishr02'],
	    'qa2':['xlqmxishr03', 'xlqmxishr04'],
	    'qa3':['xlqmxishr05', 'xlqmxishr06', 'xlqmxishr07', 'xlqmxishr08'],
	    'qa4':['xlqmxishr09', 'xlqmxishr10'],
	}
}

env_mappings_mtx = {
    'dev':{
        'dev1':['xldmxishr05'],
        'dev2':['xldmxishr06'],
        'dev3':['xldmxishr11'],
        'dev4':['xldmxishr13']
        },
    'itest-srch':{
        'itest1':['xltmxishr01', 'xltmxishr02'],
        'itest2':['xltmxishr03', 'xltmxishr04'],
        'itest3':['xltmxishr05', 'xltmxishr06'],
        'itest4':['xltmxishr07', 'xltmxishr08'],
        'itest5':['xltmxishr09', 'xltmxishr10'],
        }
}
#---------------------------------------------
cmd_ROOT_cert = "cd /SWA/mxmtxid/certs/ ; find . -name 'SWADEVROOT*.cer' -exec openssl x509 -enddate -noout -in {} \;"

#cmd_ROOT_cert_test = "cd /SWA/mxmtxid/certs/ ; find . -name 'SWADEVROOT*.cer' -exec openssl x509 -checkend 86400 -noout -in {} \;" 

cmd_SWADEV1_cert = "cd /SWA/mxmtxid/certs ; find . -name 'SWADEVISS*1v1.cer' -exec openssl x509 -enddate -noout -in {} \;"  

cmd_SWADEV2_cert = "cd /SWA/mxmtxid/certs ; find . -name 'SWADEVISS*2v1.cer' -exec openssl x509 -enddate -noout -in {} \;" 

cmd_ROOT_QA = "cd /SWA/mxmtxid/certs/ ; find . -name 'WQISSUINGCA*.cer' -exec openssl x509 -enddate -noout -in {} \;"

cmd_host_cert = "cd /SWA/mxmtxid/certs/ ; find . -name `hostname`.crt -exec openssl x509 -enddate -noout -in {} \;"


cmd_dev_asb = "cd /SWA/mxmtxid/asb-3.5.0.7*/server ; find . -maxdepth 1 -name 'SWADEVI*.cer' -exec openssl x509 -enddate -noout -in {} \;" #this is also correct

cmd_itest_asb = "cd /SWA/mxmtxid/asb-3.5.0.7*/server ; find . -maxdepth 1 -name 'SWADEVI*.cer' -exec openssl x509 -enddate -noout -in {} \;"

cmd_qa_asb = "cd /SWA/mxmtxid/asb-3.5.0.7*/server ; find . -maxdepth 1 -name 'maintenix-qa*.cert' -exec openssl x509 -enddate -noout -in {} \;"

cmd_jasper_cert = "print 'jasper cert!'"

print 'variables added'
print 'todays date: ',
time = time.strftime("Month:%m Day:%d---Time:%H:%M:%S:----Year:%Y")
print time
print'-----------------------------------------------------'
t0format = "%b %d %H:%M:%S %Y"
today = datetime.datetime.today()
print 'ISO   :', today
s = today.strftime(t0format)
print 'strftime:', s 
d = datetime.datetime.strptime(s, t0format)
print 'strptime:', d.strftime(t0format)
#-------------------Cert Checker Body----------------------------

def certs_checker(hostname, environment):
    fail = 0
    client = paramiko.Transport((hostname,port)) 
    client.connect(username=username, password=password)
    #print 'logging in'
    #print ' connection made'
    print 'Environment: %s; Hostname: %s; ' % (environment, hostname)
#------------------------Root cert--
    session = client.open_channel(kind='session')
    if "shr" in hostname:
        ROOT_certs(session, hostname)

#-----------------SWADEV1_cert------
    #print command1
    session = client.open_channel(kind='session')
    if "xld" in hostname or "xlt" in hostname:
        #print "dev or itest sol"
        SWADEV1_cert(session, hostname)
    #else:
        #print 'Does not have solr'

#----------------------SWADEV2_cert----------
    session = client.open_channel(kind='session')
    if "xlt" in hostname or "xld" in hostname:
        #print "dev or itest sol"
        SWADEV2_cert(session, hostname)
    #else:
        #print 'Does not have solr'

#----------------------host_cert----------
    session = client.open_channel(kind='session')
    if "xlt" in hostname or "xld" in hostname or "xlq" in hostname:
        #print "dev or itest sol"
        host_cert(session, hostname)
    #else:
        #print 'Does not have solr'

#----------------------ROOT_QA_Cert----------
    #session = client.open_channel(kind='session')
    #if "xlq" in hostname:
        #print "dev or itest sol"
        #ROOT_QA(session, hostname)
    #else:
        #print 'Does not have solr'
        


    #print 'next server'
    print '-----------------------------------------------------'
    #print ' '
#------------------------------end certs_checker definition-------------



#---------------SWA certs check--------------------------
def ROOT_certs(session, hostname):
    timeout=None
    stdout_data = []
    stderr_data = []
    fail = 0
    if 'xld' in hostname:
        print 'SWA cert: SWADEVROOT*.cer',
        session.exec_command(cmd_ROOT_cert)
    if 'xlt' in hostname:
        print 'SWA cert: SWADEVROOT*.cer',
        session.exec_command(cmd_ROOT_cert)
    if 'xlq' in hostname:
        print 'SWA cert: WQISSUINGCA*.cer -',
        session.exec_command(cmd_ROOT_QA)
    #print 'Session command'
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 100:
                break
    # print environment
    #print 'exit status: ', session.recv_exit_status()
    ROOT_cert = ''.join(stdout_data)
    ROOT_cert.split()
    #ROOT_cert_test = ''.join(stdout_data)
    #ROOT_cert_test.split()
    #ROOT_cert = ROOT_cert.replace(' ','')
    # print 'SWA cert: SWADEVROOT*.cer -',
    if ROOT_cert == '':
        print 'No Cert found'
    else:
        print ROOT_cert
    #root_cert_date = ROOT_cert.replace('notAfter=','')
    #root_cert_date = root_cert_date.replace(' GMT', '')
    #print root_cert_date,
    
        #print ' '
   # print 'ROOT_cert_test:  '
   # print ROOT_cert_test,
   #if ROOT_cert_test == '0'
       # print "Cert is good for another day"
    #else:
        #print "expired or not found"
#-------------------------SWADEV method--------------------
def SWADEV1_cert(session, hostname):
    timeout=None
    stdout_data = []
    stderr_data = []
    fail = 0
    #print cmd_SWADEV1_cert
    session.exec_command(cmd_SWADEV1_cert)
    #print 'Session command'
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 100:
                break
    # print environment
    #print 'exit status: ', session.recv_exit_status()
    SWADEV1_cert = ''.join(stdout_data)
    SWADEV1_cert.split()
    #SWADEV1_cert = SWADEV1_cert.replace(' ','')
    print 'Solr cert: SWADEVISS*1v1.cer -',
    if SWADEV1_cert == '':
        print 'No Cert found'
    else:
        print SWADEV1_cert,
        #print ' '

#--------------------------cmd_SWADEV_cert2----------------------------
def SWADEV2_cert(session, hostname):
    timeout=None
    stdout_data = []
    stderr_data = []
    fail = 0
    #print cmd_SWADEV1_cert
    session.exec_command(cmd_SWADEV2_cert)
    #print 'Session command'
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 100:
                break
    # print environment
    #print 'exit status: ', session.recv_exit_status()
    SWADEV2_cert = ''.join(stdout_data)
    SWADEV2_cert.split()
    #SWADEV2_cert = SWADEV2_cert.replace(' ','')
    print 'Solr cert: SWADEVISS*2v1.cer -',
    if SWADEV2_cert == '':
        print 'No Cert found'
    else:
        print SWADEV2_cert,
        #print ' '

#--------------------------cmd_ROOT_QA----------------------------
def ROOT_QA(session, hostname):
    timeout=None
    stdout_data = []
    stderr_data = []
    fail = 0
    #print cmd_ROOT_QA
    session.exec_command(cmd_ROOT_QA)
    #print 'Session command'
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 100:
                break
    # print environment
    #print 'exit status: ', session.recv_exit_status()
    ROOT_QA = ''.join(stdout_data)
    ROOT_QA.split()
    #SWADEV2_cert = SWADEV2_cert.replace(' ','')
    print 'Solr cert: WQISSUINGCA*.cer -',
    if ROOT_QA == '':
        print 'No Cert found'
    else:
        print ROOT_QA,
        #print ' '

#--------------------------cmd_host_cert----------------------------
def host_cert(session, hostname):
    timeout=None
    stdout_data = []
    stderr_data = []
    fail = 0
    #print cmd_host_cert
    session.exec_command(cmd_host_cert)
    #print 'Session command'
    while True:
        #print 'enter while loop'
        if session.recv_ready():
            stdout_data.append(session.recv(nbytes))
        if session.recv_stderr_ready():
            stderr_data.append(session.recv_stderr(nbytes))
        if session.exit_status_ready():
            fail = fail + 1
            if fail > 100:
                break
    # print environment
    #print 'exit status: ', session.recv_exit_status()
    host_cert = ''.join(stdout_data)
    host_cert.split()
    #SWADEV2_cert = SWADEV2_cert.replace(' ','')
    print 'Host cert: %s.crt  -' % (hostname),
    if host_cert == '':
        print 'No Cert found'
    else:
        print host_cert,
        #print ' '
  
#----------------------MAIN-----------------------------------------------------------------

for stage, environments in env_mappings_mtx_all.iteritems():
    for environment, hosts in environments.iteritems():
        for host in hosts:
            certs_checker(host, environment)







